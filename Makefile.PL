use 5.014000;
use ExtUtils::MakeMaker;

WriteMakefile(
	NAME              => 'App::Web::Oof',
	VERSION_FROM      => 'lib/App/Web/Oof.pm',
	ABSTRACT_FROM     => 'lib/App/Web/Oof.pm',
	AUTHOR            => 'Marius Gavrilescu <marius@ieval.ro>',
	MIN_PERL_VERSION  => '5.14.0',
	LICENSE           => 'perl',
	SIGN              => 1,
	PREREQ_PM         => {
		qw/Plack::App::File       0
		   Plack::Builder         0
		   DBIx::Simple           0
		   Email::Sender::Simple  0
		   Email::Simple          0
		   File::Slurp            0
		   HTML::TreeBuilder      0
		   HTML::Element::Library 0
		   JSON::MaybeXS          0
		   Text::CSV              0
		   Try::Tiny              0/,
	},
	META_ADD         => {
		dynamic_config => 0,
		resources      => {
			repository => 'https://git.ieval.ro/?p=app-web-oof.git',
		},
	}
);
