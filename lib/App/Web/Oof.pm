package App::Web::Oof;

use 5.014000;
use strict;
use warnings;
use utf8;
use parent qw/Plack::Component/;

our $VERSION = '0.000_008';

use DBIx::Simple;
use Email::Sender::Simple 'sendmail';
use Email::Simple;
use File::Slurp;
use HTML::TreeBuilder;
use HTML::Element::Library;
use JSON::MaybeXS qw/encode_json decode_json/;
use Plack::Builder;
use Plack::Request;
use Text::CSV;
use Try::Tiny;

sub HTML::Element::iter3 {
	my ($self, $data, $code) = @_;
	my $orig = $self;
	my $prev = $orig;
	for my $el (@$data) {
		my $current = $orig->clone;
		$code->($el, $current);
		$prev->postinsert($current);
		$prev = $current;
	}
	$orig->detach;
}

sub HTML::Element::fid    { shift->look_down(id    => shift) }
sub HTML::Element::fclass { shift->look_down(class => qr/\b$_[0]\b/) }

##################################################

my $postage_base = $ENV{OOF_POSTAGE_BASE} // 225;
my $postage_per_item = $ENV{OOF_POSTAGE_PER_ITEM} // 50;

##################################################

my %db;
my ($form, $continue, $order, $details, $pay, $display, $down);

{
	sub parse_html {
		my $builder = HTML::TreeBuilder->new;
		$builder->ignore_unknown(0);
		$builder->parse_file("tmpl/$_[0].html");
		$builder
	}

	$form     = parse_html 'form';
	$continue = parse_html 'continue';
	$order    = parse_html 'order';
	$details  = parse_html 'details';
	$pay      = parse_html 'pay';
	$display  = parse_html 'display';
	$down     = parse_html 'down';
}

sub stringify_money { sprintf "£%.2f", $_[0] / 100 }

sub make_slug {
	my $slug = $_[0];
	$slug =~ y/ /-/;
	$slug =~ y/a-zA-Z0-9-//cd;
	$slug
}

sub product_to_schemaorg {
	my ($include_url, %data) = @_;
	my $stock = $data{stock} > 0 ? 'InStock' : 'OutOfStock';
	my @extra;
	push @extra, (brand => {'@type' => 'Brand', name => $data{brand}}) if $data{brand};
	push @extra, (model => $data{model}) if $data{model};
	+{
		'@context' => 'http://schema.org/',
		'@type'    => 'Product',
		name => $data{title},
		image => "/static/fullpics/$data{product}-1.jpg",
		description => $data{subtitle},
		@extra,
		offers => {
			'@type' => 'Offer',
			price => ($data{price} =~ s/(..)$/\.$1/r),
			priceCurrency => 'GBP',
			availability => "http://schema.org/$stock",
			($include_url ? (url => "/details/$data{product}/" . make_slug $data{title}) : ())
		}
	}
}

our %highlight;
sub form_table_row {
	my ($data, $tr) = @_;
	$tr->attr(class => 'highlight') if $highlight{$data->{product}};
	$tr->fclass($_)->replace_content($data->{$_}) for qw/title subtitle stock/;
	$tr->fclass('price')->replace_content(stringify_money $data->{price});
	$tr->fclass('freepost')->detach unless $data->{freepost};
	$tr->fclass('title')->attr('data-product', $data->{product});
	$tr->fclass('title')->attr('href', '/details/'.$data->{product}.'/'.make_slug $data->{title});
#	$tr->fclass('title')->attr('data-summary', $data->{summary});
	$tr->look_down(_tag => 'input')->attr(max => $data->{stock});
	$tr->look_down(_tag => 'input')->attr(name => 'quant'.$data->{product});
}

sub form_app {
	my ($env) = @_;
	$db{$$} //= DBIx::Simple->connect($ENV{OOF_DSN} // 'dbi:Pg:');
	my $req = Plack::Request->new($env);

	local %highlight = map { $_ => 1 } $req->param('highlight');
	my $data = $db{$$}->select(products => '*', {stock => {'>', 0}}, 'product')->hashes;
	my $tree = $form->clone;
	$tree->find('tbody')->find('tr')->iter3($data, \&form_table_row);

	[200, ['Content-type' => 'text/html; charset=utf-8'], [$tree->as_HTML]]
}

sub continue_table_row {
	my ($data, $tr) = @_;
	$tr->fclass($_)->replace_content($data->{$_}) for qw/title subtitle quantity/;
	$tr->fclass('freepost')->detach unless $data->{freepost};
	$tr->fclass('price')->replace_content(stringify_money $data->{subtotal});
	$tr->fclass('title')->attr('data-product', $data->{product});
}

sub continue_app {
	my ($env) = @_;
	$db{$$} //= DBIx::Simple->connect($ENV{OOF_DSN} // 'dbi:Pg:');
	my $tree = $continue->clone;
	my $req = Plack::Request->new($env);
	my $params = $req->body_parameters;

	my ($quant, $quant_freepost, $total, @data, @notes) = (0) x 3;
	for (sort keys %$params) {
		next unless /^quant/;
		next unless $params->{$_};
		my $data = $db{$$}->select(products => '*', {product => substr $_, 5})->hash;
		$data->{quantity} = $params->{$_};
		if ($data->{stock} == 0) {
			push @notes, 'Item is out of stock and was removed from order: '.$data->{title};
			next
		}
		if ($data->{quantity} > $data->{stock}) {
			$data->{quantity} = $data->{stock};
			push @notes, 'Not enough units of "'.$data->{title}.'" available. Quantity reduced to '.$data->{quantity}
		}
		$data->{subtotal} = $data->{price} * $data->{quantity};
		$quant += $data->{quantity};
		$quant_freepost += $data->{quantity} if $data->{freepost};
		$total += $data->{subtotal};
		push @data, $data
	}

	return [500, ['Content-type' => 'text/plain'], ['Error: no items in order.']] unless $quant;

	$tree->fid('subtotal')->replace_content(stringify_money $total);
	my $dvalue;
	if ($params->{discount}) {
		my $discount = $db{$$}->select(discounts => '*', {discount => $params->{discount}})->hash;
		if (!defined $discount) {
			push @notes, 'Discount code incorrect. No discount applied.'
		} elsif ($db{$$}->select(orders => 'COUNT(*)', {discount => $params->{discount}})->list) {
			push @notes, 'Discount code already used once. No discount applied.'
		} else {
			$dvalue = int (0.5 + $discount->{fraction} * $total) if $discount->{fraction};
			$dvalue = $discount->{flat}                          if $discount->{flat};
			$tree->fid('discount')->replace_content('-'.stringify_money $dvalue);
			$total -= $dvalue;
			$tree->look_down(name => 'discount')->attr(value => $params->{discount});
			push @notes, 'Discount applied.'
		}
	}
	$tree->look_down(name => 'discount')->detach unless $dvalue;
	$tree->fid('discount_tr')->detach unless $dvalue;
	my $postage = $postage_base + $postage_per_item * ($quant - $quant_freepost);
	$postage = 0 if $quant == $quant_freepost;
	$tree->fid('postage')->replace_content(stringify_money $postage);
	$total += $postage;
	$tree->fid('total')->replace_content(stringify_money $total);

	$tree->fid('order')->find('tbody')->find('tr')->iter3(\@data, \&continue_table_row);
	$tree->iter($tree->fid('notes')->find('li') => @notes);

	$tree->look_down(name => 'products')->attr(value => encode_json \@data);
	$tree->look_down(name => 'total')->attr(value => $total);

	[200, ['Content-type' => 'text/html; charset=utf-8'], [$tree->as_HTML]]
}

sub order_app {
	my ($env) = @_;
	$db{$$} //= DBIx::Simple->connect($ENV{OOF_DSN} // 'dbi:Pg:');
	my $tree = $order->clone;
	my $req = Plack::Request->new($env);
	my ($id) = $env->{PATH_INFO} =~ m,^/([0-9A-F]+),;
	if ($id) {
		my $total = $db{$$}->select(orders => 'total', {id => $id})->list or
		  return [500, ['Content-type', 'text/plain'], ['Order not found']];
		$tree->fid('orderid')->replace_content($id);
		$tree->look_down(name => 'order')->attr(value => $id);
		$tree->fid('total')->replace_content(stringify_money $total);
		$tree->find('script')->attr('data-amount', $total);
		return [200, ['Content-type' => 'text/html; charset=utf-8'], [$tree->as_HTML]]
	} else {
		my %parms = %{$req->body_parameters};
		my $id = sprintf "%X%04X", time, $$;
		my $err;
		try {
			$db{$$}->begin_work;
			my $products = decode_json $req->body_parameters->{products};
			for my $prod (@$products) {
				my $stock = $db{$$}->select(products => 'stock', {product => $prod->{product}})->list;
				die "Not enough of " .$prod->{title}."\n" if $prod->{quantity} > $stock;
				$db{$$}->update(products => {stock => $stock - $prod->{quantity}}, {product => $prod->{product}});
			}
			$db{$$}->insert(orders => {id => $id, date => time, %parms});
			$db{$$}->commit;
			sendmail (Email::Simple->create(
				header => [
					From    => $ENV{OOF_EMAIL_FROM},
					To      => $ENV{OOF_EMAIL_TO},
					Subject => "Order $id placed for ".stringify_money($parms{total}),
				],
				body => 'A new order was placed.',
			)) if $ENV{OOF_EMAIL_TO};
		} catch {
			$db{$$}->rollback;
			$err = [500, ['Content-type', 'text/plain'], ["Error: $_"]]
		};
		return $err if $err;
		return [303, [Location => "/order/$id"], []]
	}
}

sub cancel {
	my ($order) = @_;
	$db{$$} //= DBIx::Simple->connect($ENV{OOF_DSN} // 'dbi:Pg:');
	$order = $db{$$}->select(orders => '*', {id => $order})->hash;
	my $products = decode_json $order->{products};
	$db{$$}->begin_work;
	try {
		for my $prod (@$products) {
			my $stock = $db{$$}->select(products => 'stock', {product => $prod->{product}})->list;
			$db{$$}->update(products => {stock => $stock + $prod->{quantity}}, {product => $prod->{product}});
		}
		$db{$$}->delete(orders => {id => $order->{id}});
		$db{$$}->commit;
	} catch {
		$db{$$}->rollback;
		die $_
	}
}

sub details_list_element {
	my ($data, $li) = @_;
	$li->find('a')->attr(href => "/$data");
	my $thumb = $data =~ s/fullpics/thumbs/r;
	$thumb = $data unless -f $thumb;
	$li->find('img')->attr(src => "/$thumb");
}

sub details_app {
	my ($env) = @_;
	$db{$$} //= DBIx::Simple->connect($ENV{OOF_DSN} // 'dbi:Pg:');
	my $tree = $details->clone;
	my ($id) = $env->{PATH_INFO} =~ m,^/(\d+),;
	my %data = %{$db{$$}->select(products => '*', {product => $id})->hash};
	my @pics = <static/fullpics/$id-*>;
	my $slug = make_slug $data{title};
	$tree->find('title')->replace_content("$data{title} | ledparts4you");
	$tree->find('h2')->replace_content($data{title});
	my $summary_literal = HTML::Element::Library::super_literal $data{summary};
	$tree->fid('summary')->replace_content($summary_literal);
	$tree->look_down(rel => 'canonical')->attr(href => "/details/$id/$slug");
	$tree->fid('pictures')->find('li')->iter3(\@pics, \&details_list_element);
	$tree->fid('jsonld')->replace_content(encode_json product_to_schemaorg '', %data);

	$tree->fid('dd_stock')->replace_content($data{stock});
	$tree->fid('dd_price')->replace_content(stringify_money $data{price});
	for (qw/brand model/) {
		if ($data{$_}) {
			$tree->fid("dd_$_")->replace_content($data{$_});
		} else {
			$tree->fid("dt_$_")->detach;
			$tree->fid("dd_$_")->detach;
		}
	}

	for my $ahref ($tree->find('a')) {
		$ahref->attr(href => "/form?highlight=$id") if $ahref->attr('href') eq '/';
	}

	[200, ['Content-type' => 'text/html; charset=utf-8'], [$tree->as_HTML]]
}

sub pay_app {
	my ($env) = @_;
	my $req = Plack::Request->new($env);
	$db{$$} //= DBIx::Simple->connect($ENV{OOF_DSN} // 'dbi:Pg:');
	my $order = $req->body_parameters->{order};
	my $token = $req->body_parameters->{stripeToken};
	return [500, ['Content-type' => 'text/html; charset=utf-8'], ['No token received, payment did not succeed.']] unless $token;
	$db{$$}->update(orders => {stripe_token => $token}, {id => $order});
	[200, ['Content-type' => 'text/html; charset=utf-8'], [$pay->as_HTML]];
}

sub display_table_row {
	my ($data, $tr) = @_;
	$tr->fclass($_)->replace_content($data->{$_}) for qw/title subtitle quantity/;
	$tr->fclass('freepost')->detach unless $data->{freepost};
	$tr->fclass('price')->replace_content(stringify_money $data->{subtotal});
	$tr->fclass('title')->attr('data-product', $data->{product});
}

sub display_order {
	my ($data, $div) = @_;
	my @products = @{decode_json $data->{products}};
	$div->find('table')->iter3(\@products, \&display_table_row);
	$div->fclass('name')->replace_content($data->{first_name} . ' ' . $data->{last_name});
	$div->fclass('stripe_token')->replace_content($data->{stripe_token}) if $data->{stripe_token};
}

my $csv_header = 'Address_line_1,Address_line_2,Address_line_3,Address_line_4,Postcode,First_name,Last_name,Email,Weight(Kg),Compensation(�),Signature(y/n),Reference,Contents,Parcel_value(�),Delivery_phone,Delivery_safe_place,Delivery_instructions' . "\n";

sub make_hermes_csv {
	my ($order) = @_;
	my $csv = Text::CSV->new;
	my @fields = map { $order->{$_} } qw/address1 address2 address3 address4 postcode first_name last_name email/;
	$csv->combine(@fields, '', '', 'n', '', '', '', $order->{phone}, $order->{safe_place}, $order->{instructions});
	$csv->string
}

sub display_app {
	my ($env) = @_;
	$db{$$} //= DBIx::Simple->connect($ENV{OOF_DSN} // 'dbi:Pg:');
	my $req = Plack::Request->new($env);
	my $n = int ($req->param('n') // 10);
	my @orders = $db{$$}->query("SELECT * FROM orders ORDER BY date DESC LIMIT $n")->hashes;
	my $tree = $display->clone;
	$tree->fclass('order')->iter3(\@orders, \&display_order);
	my $csv = join "\n", map { make_hermes_csv $_ } @orders;
	$tree->fid('hermes_csv')->replace_content($csv_header . $csv);
	[200, ['Content-type' => 'text/html; charset=utf-8'], [$tree->as_HTML]];
}

sub app {
	my $footer = read_file 'tmpl/footer.html';
	builder {
		enable sub {
			my $app = shift;
			sub {
				my $res = $app->(@_);
				$res->[2][0] =~ s,</body>,$footer</body>, if $res->[0] == 200;
				$res;
			}
		};
		enable sub {
			my $app = shift;
			sub {
				if (-f 'down.html') {
					my $down_lit = HTML::Element::Library::super_literal read_file 'down.html';
					my $tree = $down->clone;
					$tree->fid('content')->replace_content($down_lit);
					return [503, ['Content-type' => 'text/html; charset=utf-8'], [$tree->as_HTML]]
				}
				$app->(@_)
			}
		};
		mount '/' => sub { [301, [Location => '/form'], []] };
		mount '/form'     => \&form_app;
		mount '/continue' => \&continue_app;
		mount '/order'    => \&order_app;
		mount '/details'  => \&details_app;
		mount '/pay'      => \&pay_app;
		mount '/display'  => \&display_app;
	}
}

1;
__END__

=head1 NAME

App::Web::Oof - Oversimplified order form / ecommerce website

=head1 SYNOPSIS

  use App::Web::Oof;

=head1 DESCRIPTION

Oof (Oversimplified order form) is a very simple ecommerce website.
It is the code behind L<https://ledparts4you.uk.to>.

This version is reasonably functional, yet not very reusable, hence
the version number.

=head1 AUTHOR

Marius Gavrilescu, E<lt>marius@ieval.roE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2016 by Marius Gavrilescu

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.22.1 or,
at your option, any later version of Perl 5 you may have available.


=cut
