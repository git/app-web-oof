#!/usr/bin/perl
use 5.014000;
use warnings;

use App::Web::Oof;
use Plack::App::File;
use Plack::Builder;

builder {
	mount '/static/' => Plack::App::File->new(root => 'static')->to_app,
	mount '/' => App::Web::Oof::app,
}
